package mk.andrej.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mk.andrej.calculator.service.CalculatorServiceImpl;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {

	@Autowired
	private CalculatorServiceImpl calculatorService;

	@PostMapping("/add")
	public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
		return calculatorService.add(a, b);
	}

	@PostMapping("/subtract")
	public Integer subract(@RequestParam Integer a, @RequestParam Integer b) {
		return calculatorService.subtract(a, b);
	}

	@PostMapping("/multiply")
	public Integer multiply(@RequestParam Integer a, @RequestParam Integer b) {
		return calculatorService.multiply(a, b);
	}

	@PostMapping("/divide")
	public Float divide(@RequestParam Integer a, @RequestParam Integer b) {
		return calculatorService.divide(a, b);
	}

	@PostMapping("/mod")
	public Integer mod(@RequestParam Integer a, @RequestParam Integer b) {
		return calculatorService.mod(a, b);
	}

}
