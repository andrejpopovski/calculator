package mk.andrej.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {
	@Override
	public Integer add(Integer a, Integer b) {
		return a + b;
	}

	@Override
	public Integer subtract(Integer a, Integer b) {
		return a - b;
	}

	@Override
	public Integer multiply(Integer a, Integer b) {
		return a * b;
	}

	@Override
	public Float divide(Integer a, Integer b) {
		return (float) a / b;
	}

	@Override
	public Integer mod(Integer a, Integer b) {
		return a % b;
	}

}
