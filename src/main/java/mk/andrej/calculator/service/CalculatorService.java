package mk.andrej.calculator.service;

public interface CalculatorService {
	public Integer add(Integer a, Integer b);

	public Integer subtract(Integer a, Integer b);

	public Float divide(Integer a, Integer b);

	public Integer multiply(Integer a, Integer b);

	public Integer mod(Integer a, Integer b);

}
